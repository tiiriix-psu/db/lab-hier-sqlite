package ru.fnight.lab2db.domains;

public class Record {

    private Integer id;
    private String entryTime;
    private Integer userId;

    public Record(Integer id, String entryTime, Integer userId) {
        this.id = id;
        this.entryTime = entryTime;
        this.userId = userId;
    }

    @Override
    public String toString() {
        return entryTime;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEntryTime() {
        return entryTime;
    }

    public void setEntryTime(String entryTime) {
        this.entryTime = entryTime;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }
}
