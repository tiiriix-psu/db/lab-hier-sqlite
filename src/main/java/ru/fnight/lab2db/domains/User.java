package ru.fnight.lab2db.domains;

public class User {

    private Integer id;
    private String login;

    public User(Integer id, String login) {
        this.id = id;
        this.login = login;
    }

    @Override
    public String toString() {
        return login;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }
}
