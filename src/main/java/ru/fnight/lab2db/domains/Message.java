package ru.fnight.lab2db.domains;

public class Message {

    private Integer id;
    private String text;
    private String time;
    private Integer entryId;

    public Message(Integer id, String text, String time, Integer entryId) {
        this.id = id;
        this.text = text;
        this.time = time;
        this.entryId = entryId;
    }

    @Override
    public String toString() {
        return text;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public Integer getEntryId() {
        return entryId;
    }

    public void setEntryId(Integer entryId) {
        this.entryId = entryId;
    }
}
