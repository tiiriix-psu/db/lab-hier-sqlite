package ru.fnight.lab2db.controllers;

import javafx.beans.property.BooleanProperty;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import ru.fnight.lab2db.domains.Message;
import ru.fnight.lab2db.domains.Record;
import ru.fnight.lab2db.domains.User;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Objects;

public class Window {

    public TreeView treeView;

    private <T, E> TreeItem<E> addTreeItem(TreeItem<T> parent, E value) {
        parent.getChildren().add((TreeItem<T>) new TreeItem<E>(value));
        return (TreeItem<E>) parent.getChildren().get(parent.getChildren().size() - 1);
    }

    private <T extends E, E> T getLastChildValue(TreeItem<E> parent) {
        return (T) parent.getChildren().get(parent.getChildren().size() - 1).getValue();
    }

    private void appendToTreeView(User user, Record record, Message message) {
        TreeItem<Object> root = treeView.getRoot();

        User prevUser = root.getChildren().size() > 0 ? getLastChildValue(root) : null;

        if (prevUser != null && record != null && Objects.equals(user.getId(), prevUser.getId())) {

            TreeItem<Object> userNode = root.getChildren().get(root.getChildren().size() - 1);

            Record prevRecord  = userNode.getChildren().size() > 0 ? getLastChildValue(userNode) : null;

            if (prevRecord != null && Objects.equals(record.getId(), prevRecord.getId())) {
                TreeItem<Object> recordNode = userNode.getChildren().get(userNode.getChildren().size() - 1);

                if (message != null) {
                    addTreeItem(recordNode, message);
                }
            } else {

                TreeItem<Object> newTimeItem = addTreeItem(userNode, record);
                if (message != null) {
                    addTreeItem(newTimeItem, message);
                }
            }
        } else {
            TreeItem<Object> newLoginItem = addTreeItem(root, user);

            newLoginItem.expandedProperty().addListener((observable, oldValue, newValue) -> {
                BooleanProperty bb = (BooleanProperty) observable;
                System.out.println("bb.getBean() = " + bb.getBean());
                TreeItem<Object> t = (TreeItem<Object>) bb.getBean();
                if (((User) t.getValue()).getId().equals(user.getId())) {
                    int allNodesWithChildren = 0;
                    int allNodesWithoutChildren = 0;
                    for (TreeItem<Object> item : t.getChildren()) {
                        if (item.getChildren().isEmpty()) {
                            allNodesWithoutChildren++;
                        } else {
                            allNodesWithChildren++;
                        }
                    }
                    t.setValue(String.format("%s - %d/%d", user.getLogin(), allNodesWithChildren, allNodesWithoutChildren));
                }
            });
            if (record != null) {
                TreeItem<Object> newRecordNode = addTreeItem(newLoginItem, record);
                if (message != null) {
                    addTreeItem(newRecordNode, message);
                }
            }
        }
    }

    public void initTreeView() {
        Connection conn = null;
        try {
            String url = "jdbc:sqlite:test_db.sqlite";
            conn = DriverManager.getConnection(url);

            String sql = "select users.id user_id, " +
                    "login, " +
                    "entry_records.id entry_id, " +
                    "entry_time, " +
                    "messages.id message_id, " +
                    "text, " +
                    "time message_time " +
                    "from users " +
                    "left join entry_records on users.id = entry_records.user_id " +
                    "left join messages on entry_id = entry_records.id " +
                    "order by user_id, entry_id";

            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            ResultSet resultSet = preparedStatement.executeQuery();

            treeView.setRoot(new TreeItem<>("Пользователи"));

            while (resultSet.next()) {
                Integer userId = resultSet.getInt("user_id");
                String login = resultSet.getString("login");
                Object entryIdObj = resultSet.getObject("entry_id");
                Integer entryId = entryIdObj instanceof Integer ? (Integer) entryIdObj : null;
                String entryTime = resultSet.getString("entry_time");
                Object messageIdObj = resultSet.getObject("message_id");
                Integer messageId = messageIdObj instanceof Integer ? (Integer) messageIdObj : null;
                String text = resultSet.getString("text");
                String messageTime = resultSet.getString("message_time");
                User user = new User(userId, login);
                Record record = entryId == null ? null : new Record(entryId, entryTime, userId);
                Message message = messageId == null ? null : new Message(messageId, text, messageTime, entryId);
                appendToTreeView(user, record, message);
            }
            treeView.setShowRoot(false);

            resultSet.close();
            preparedStatement.close();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }
    }
}
